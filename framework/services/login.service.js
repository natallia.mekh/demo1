import { buildRequest } from '../../lib/index';

import { decorateService } from '../../lib/index';

const LoginVikunja = function LoginVikunja() {
  this.post = async function loginVikunja(params) {
    reporter.startStep('Вызываем /api/v1/login');
    // expect that it's fancy

    const req  = buildRequest;
    const r = await req().post('/api/v1/login').send(params);
    reporter.addAttachment("Это лог", 'Это сам текст', "text/plain");

    reporter.endStep();
    return r;
  };
};
decorateService(LoginVikunja);

export { LoginVikunja };
